package exemple4;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Random;

public class Main {
	private static final int N_VOLS = 10;
	private static final Random random = new Random();
	private VolReal[] vols = new VolReal[N_VOLS];

	public static void main(String[] args) {
		Main main = new Main();
		main.mostraVols();
		// Ordenem per sortides
		/*
		 * En aquest exemple veiem com les funcions lambda també es poden utilitzar
		 * directament, cosa que pot ser útil en aquest cas si volem utilitzar un
		 * mètode de comparació que no ha estat implementat a la classe VolReal.
		 */
		Arrays.sort(main.vols, (v1, v2) -> v1.getSortida().compareTo(v2.getSortida()));
		main.mostraVols();
		// Ordenem per arribades
		Arrays.sort(main.vols, (v1, v2) -> v1.getArribada().compareTo(v2.getArribada()));
		main.mostraVols();
	}
	
	public Main() {
		ZonedDateTime sortida, arribada;
		
		for (int i=0; i<vols.length; i++) {
			sortida = ZonedDateTime.now().plusMinutes(random.nextInt(60)).truncatedTo(ChronoUnit.MINUTES);
			arribada = sortida.plusMinutes(random.nextInt(10*60));
			vols[i] = new VolReal(sortida, arribada);
		}
	}
	
	public void mostraVols() {
		for (VolReal v : vols) 
			System.out.println(v.getSortida()+" - "+v.getArribada());
		System.out.println();
	}
}

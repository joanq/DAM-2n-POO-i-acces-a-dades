package zoo;

public abstract class Animal implements Esser {
	public abstract String mou(Zoo z);
	public abstract String alimenta(Zoo z);
	public abstract String expressa(Zoo z);
	public String accio(Zoo z) {
		int num = Zoo.r.nextInt(3);
		String s;
		switch (num) {
		case 0:
			s = mou(z);
			break;
		case 1:
			s = alimenta(z);
			break;
		default:
			s = expressa(z);
		}
		return s;
	}
}

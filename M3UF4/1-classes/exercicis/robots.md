## Els robots

Els robots de la classe *MarkJava* van ser dels més famosos de la història.
N'hi havia de tres tipus: els *Destructors*, els *Replicants* i els *Mecanics*.
Tots els robots *MarkJava* estaven alimentats per una bateria que els donava
energia. Els robots només podien fer les seves accions si tenien prou energia.
Els *Destructors* buidaven les bateries del seus enemics, deixant-los immòbils,
els *Replicants* eren capaços de construir altres robots i els *Mecanics* eren
capaços de recarregar la seva pròpia bateria i la dels altres robots.

### Classe *MarkJava*

Dissenya la classe MarkJava amb les següents propietats:

#### Atributs

* `Random random`: s'utilitzarà per generar nombres aleatoris quan calgui.

* `int energia`: l'energia que té el robot per seguir actuant. Tots els
robots comencen amb una energia de 10.

* `boolean haMogut`: indicarà si ja ha mogut o no durant un torn.

#### Mètodes

* `int obteEnergia()`: retorna l'atribut *energia*.

* `boolean decideixSiMou()`: torna *true* si el robot es vol moure i *false*
si no, però cada tipus ho decideix de forma diferent. Tots els robots però,
retornaran directament *false* si ja han mogut aquest torn (*haMogut* és
*true*), i posaran a *true* l'atribut *haMogut* cada cop que es cridi aquest
mètode.

* `void gastaEnergia(int energiaGastada) throws IllegalArgumentException`:
aquest mètode resta l'*energiaGastada* de l'*energia* del robot. Si
l'*energia* queda menor que 0 s'ha de llançar una excepció de tipus
*IllegalArgumentException*. Això no hauria de passar mai, si passa significa
que hi ha un error en el programa. Sempre que un robot gasti energia ho farà
cridant a aquest mètode.

* `void gastaBateria()`: aquest mètode deixa l'energia a 0.

* `void interactua(MarkJava unAltreRobot)`: aquest mètode defineix què passa
quan aquest robot en troba un altre, però cada tipus fa coses diferents.

* `void recarregaBateria()`: torna la bateria al seu valor màxim.

### Classe *Destructor*

Dissenya la classe *Destructor* amb les següents propietats:

#### Mètodes

* `void interactua(MarkJava unAltreRobot)`: si el robot té menys de 5
d'energia no fa res i es retorna del mètode. En cas contrari, el robot
*Destructor* ataca als altres robots quan els troba. Si l'atac té èxit es
crida el mètode *gastaBateria* de l'altre robot. Si el robot que ha trobat
és de tipus *Destructor*, hi ha un 50% de possibilitats que l'atac tingui
èxit. Si el robot és de qualsevol altre tipus, l'atac sempre té èxit. En
qualsevol cas, si el robot ataca, perd 3 punts d'energia.

* `boolean decideixSiMou()`: si l'energia és menor de 5, el robot no es mou.
Si no, es treu un nombre aleatori entre 1 i 10. Si el resultat és menor o
igual que 4 es retorna *true*, en cas contrari es retorna *false*.

### Classe *Mecànic*

Dissenya la classe *Mecanic* amb les següents propietats:

#### Mètodes

* `void interactua(MarkJava unAltreRobot)`: si té una energia superior a 4,
crida el mètode *recarregaBateria* de l'altre robot i perd un punt d'energia.
En cas contrari, no fa res.

* `boolean decideixSiMou()`: si l'energia és menor de 5, el robot no es mou,
però crida el seu mètode *recarregaBateria*. Si no, es treu un nombre aleatori
entre 1 i 10. Si el resultat és menor o igual que 6 es retorna *true*, en cas
contrari es retorna *false*.

### Classe *Replicant*

Dissenya la classe *Replicant* amb les següents propietats:

#### Atributs

* `MarkJava robotQueReplicara`: aquest atribut guardarà l'últim robot amb qui
s'hagi trobat, per saber de quin tipus serà el robot que faci quan en creí un.

#### Mètodes

* `void interactua(MarkJava unAltreRobot)`: assigna el robot trobat a
*robotQueReplicara*.

* `boolean decideixSiMou()`: si l'energia és menor de 7, el robot no es mou.
Si no, es treu un nombre aleatori entre 1 i 10. Si el resultat és menor o
igual que 6 es retorna *true*, en cas contrari es retorna *false*.

* `MarkJava construeix()`: si l'energia és menor de 7 retorna *null*. Si no,
crea un nou robot del mateix tipus que el que tingui guardat a
*robotQueReplicara* i el retorna. Si *robotQueReplicara* val *null*, crea un
robot de tipus aleatori (tots tenen la mateixa probabilitat). Si ha construït
algun robot, resta 5 a l'energia.

### Classe *Taller*

La classe *Taller* té les següents propietats:

#### Atributs

* `MarkJava[][] robots`: aquesta matriu representa el taller i té unes
dimensions de 5x5. A cada posició hi podrà haver un robot i valdrà *null*
si no hi ha cap robot en aquella posició.

#### Mètodes

* `void torn()`: recorre una vegada la matriu robots, per cada robot es crida
el seu mètode *decideixSiMou* i es mira el resultat. Si és *true* es crida
el mètode *resolMoviment* amb el robot que s'ha de moure i la seva posició
com a paràmetres. Quan s'ha recorregut tota la matriu, es torna a recórrer,
posant l'atribut *haMogut* de tots els robots a *false*. Finalment, es crida
el mètode *mostraTaller*.

* `void resolMoviment(MarkJava robotAMoure, int x, int y)`: es treu un número
aleatori del 0 al 3 per saber en quina direcció es vol moure el robot. El 0
vol dir dreta, l'1 vol dir avall, el 2 esquerra i el 3 amunt. Si degut al
seu moviment el robot queda fora del taller, el robot no es mou. En cas
contrari, es resta 1 d'energia al robot i es mira què hi ha a la casella
on vol anar. Si no hi ha res, es canvia la posició del robot a la nova
casella. Si hi ha un altre robot, es crida al mètode interactuar del
*robotAMoure* passant com a paràmetre el robot que s'ha trobat. En qualsevol
cas, si un robot topa amb un altre, no es mou. A més, si no hi ha res a la
casella on es vol moure i el robot és de tipus *Replicant*, es crida el
mètode *creaUnNouRobot*, passant com a paràmetres el robot constructor i
les coordenades on es vol construir el nou robot, que són les que ocupava
el robot fins llavors.

* `void mostraTaller()`: mostra l'estat del taller per pantalla. Això
significa que es dibuixarà un quadrat amb lletres per pantalla on cada lletra
representa una posició de la matriu robots. Un **-** indica que la posició és
buida, una **D** indica un destructor, una **R** un replicant i una
**M** un mecànic.

* `void creaUnNouRobot(Replicant robotQueCrea, int x, int y)`: s'assigna a
*robots[x][y]* el resultat de cridar el mètode *construeix* de *robotQueCrea*.

* `creaTaller()`: crea a la classe *Taller* un mètode *creaTaller* que posi
alguns robots en ell (la posició, tipus i quantitat podeu posar la que
vulgueu, però almenys un robot de cada tipus), que mostri l'estat inicial
del taller i que cridi el mètode *torn* d'aquest taller 10 vegades.

* `public static void main(String[] args)`: el mètode *main* crea un objecte
de tipus *Taller* i crida el seu mètode *creaTaller*.

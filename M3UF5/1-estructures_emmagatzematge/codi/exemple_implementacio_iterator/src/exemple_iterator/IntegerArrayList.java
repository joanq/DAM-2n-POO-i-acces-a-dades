package exemple_iterator;

public class IntegerArrayList implements LaMevaList {
	private int[] array = new int[100];
	private int size;
	
	@Override
	public void add(int n) {
		array[size]=n;
		size++;
	}

	@Override
	public ElMeuIterator iterator() {
		return new IntegerArrayListIterator(this);
	}
	
	private class IntegerArrayListIterator implements ElMeuIterator {
		private int pos;
		
		private IntegerArrayList list;
		
		public IntegerArrayListIterator(IntegerArrayList list) {
			this.list = list;
		}

		@Override
		public boolean hasNext() {
			return pos < list.size;
		}

		@Override
		public int next() {
			int n = list.array[pos];
			pos++;
			return n;
		}
		
	}

}

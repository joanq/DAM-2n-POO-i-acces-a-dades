package exemple_iterator;

public class IntegerLinkedList implements LaMevaList {
	private Node top;
	private Node bottom;
	
	/* (non-Javadoc)
	 * @see exemple_iterator.LaMevaList#add(int)
	 */
	@Override
	public void add(int n) {
		Node newNode = new Node(n);
		if (top==null) {
			top=bottom=newNode;
		} else {
			newNode.prev = bottom.prev;
			bottom.next = newNode;
			bottom = newNode;
		}
	}
	
	/* (non-Javadoc)
	 * @see exemple_iterator.LaMevaList#iterator()
	 */
	@Override
	public ElMeuIterator iterator() {
		return new IntegerLinkedListIterator(top);
	}
	
	private class Node {
		int n;
		Node next;
		Node prev;
		
		public Node(int n) {
			this.n=n;
		}
	}
	
	private class IntegerLinkedListIterator implements ElMeuIterator {
		private Node pos;
		
		public IntegerLinkedListIterator(Node top) {
			pos = top;
		}

		@Override
		public boolean hasNext() {
			return pos != null;
		}

		@Override
		public int next() {
			int n = pos.n;
			pos=pos.next;
			return n;
		}
		
	}
}

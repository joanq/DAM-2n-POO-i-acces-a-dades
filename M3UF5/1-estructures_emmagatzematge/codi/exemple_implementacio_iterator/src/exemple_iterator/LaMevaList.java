package exemple_iterator;

public interface LaMevaList {

	void add(int n);

	ElMeuIterator iterator();

}
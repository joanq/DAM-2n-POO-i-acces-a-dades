## La fàbrica de naus

En aquest programa dissenyarem una fàbrica de naus espacials. Cada nau estarà
formada per diversos components, que podrem instal·lar uns dins dels altres.

El programa ens facilitarà el càlcul de les característiques de la nau, i
l'obtenció d'una descripció de tots els components que té una nau. Gràcies
al disseny que farem, basat en el *Composite Pattern*, serà fàcil crear
nous components per a les naus i construir noves naus amb diferents
combinacions.

### Classe *ComponentNau*

La classe *ComponentNau* serà la base de tota la resta de components i
definirà les característiques comunes de tots els components.

Tindrà 4 atributs: el nom del component, el seu pes, la seva capacitat
(és a dir, quants components hi caben a dins), i la seva potència.

Aquests quatre atributs es podran llegir des de qualsevol lloc del programa.
El nom també es podrà modificar obertament, però els altres tres només podran
ésser modificats des de les classes derivades de *ComponentNau*.

A més, els mètodes *getPesTotal* i *getPotenciaTotal* retornaran el pes i
potència del component conjuntament a tots els components que aquest
contingui. En aquesta implementació base, simplement retornaran el pes i
potència del component, respectivament.

El constructor de *ComponentNau* rebrà el pes del component.

A més d'això, la classe *ComponentNau* tindrà una colla de mètodes sense
implementació per defecte, que serviran per gestionar el conjunt de
components inclosos a aquest component. Aquests mètodes són *add*, *remove*
i *size*, que tindran la mateixa declaració que els mètodes corresponents
d'un conjunt (*Set*).

A més, el mètode *getComponents*, que tampoc tindrà codi per defecte,
retornarà un objecte de tipus conjunt de components, que s'utilitzarà per
poder recórrer tots els components inclosos en algun component.

Finalment, el mètode *descriu*, també sense implementació, retornarà una
cadena que descriurà el component i tots els components inclosos.

### Classe *ComponentSimple*

La classe *ComponentSimple* proporcionarà una implementació base per
aquells components que no permeten tenir components inclosos al seu
interior.

El constructor rebrà el pes del component.

Els mètodes per gestionar els components inclosos llançaran tots una
excepció de tipus *UnsupportedOperationException*, indicant la
impossibilitat d'afegir components a l'interior d'un component d'aquest
tipus.

Per acabar, el mètode *descriu* retornarà la cadena que descriu aquest
objecte, amb un final de línia addicional al final.

### Classe *ComponentCompost*

La classe *ComponentCompost* serà la base de tots els components que sí
que permeten tenir altres components a dins. Com a atribut, tindrà un
conjunt components, on es posaran els components inclosos en aquest.

El constructor rebrà la capacitat del component i el seu pes.

Els mètodes *add*, *remove* i *size* cridaran als mètodes equivalents
del conjunt. L'única diferència serà en el mètode *add*, on es comprovarà
que la quantitat total de components no sobrepassi la capacitat màxima i,
si això passa, es retornarà *false* sense afegir el component.

El mètode *getComponents* retornarà una vista inalterable del conjunt
de components (veure la classe *Collections*).

El mètode *descriu* construirà una cadena amb el següent format:

```
<cadena que defineix l'objecte> amb contingut:\n
<espai><descripció component><espai><descripció component><...>Final contingut de <nom component>\n
```

Els mètodes *getPesTotal* i *getPotenciaTotal* sumaran al pes i potència
del component, el pes i potència de tots els components inclosos.

### Classe *Nau*

Un cop muntada la infraestructura, anem a implementar components concrets.
El primer de tots és la classe *Nau*, que evidentment permetrà tenir
altres components a dins.

El constructor de *Nau* rebrà el seu nom, la seva capacitat i el seu pes.

El mètode *toString* d'una nau retornarà una cadena amb les
característiques de la nau:

```
Nau <nom>; capacitat: <capacitat>, pes: <pes>
```

Canviarem el comportament per defecte del mètode *add* per garantir que
no posem una nau com a component d'una altra nau. Si això s'intenta, es
llançarà una excepció de tipus *IllegalArgumentException*.

### Classe *Motor*

La classe *Motor* serà un exemple de component simple, que no admet
components inclosos.

El seu constructor rebrà la potència del motor i el seu pes.

El mètode *toString* retornarà una cadena en el format:

```
<nom>; Potència: <potència>, pes: <pes>
```

### Classe *Habitacle*

La classe *Habitacle* serà un altre exemple de component simple. Tindrà
un atribut que determinarà la quantitat màxima d'habitants que poden
veure-hi, i que rebrà amb el constructor conjuntament amb el pes del
component.

El seu mètode *toString* tornarà una cadena en el format:

```
<nom>; habitants màxims: <habitants màxims>, pes: <pes>
```

### Classe *Hangar*

La classe *Hangar* serà un component que permet tenir-ne d'altres a dins.
Rebrà la capacitat màxima i el pes en el constructor.

Cal fer un parell de modificacions sobre el comportament per defecte. Per
una banda, el mètode *add* s'ha d'assegurar que només es poden posar a
l'hangar naus, i llançar una excepció de tipus *IllegalArgumentException*
si s'intenta posar un component d'un altre tipus.

Per altra banda, la potència de les naus que hi ha l'hangar no se sumen a
la potència total de la nau principal, així que s'ha de modificar el mètode
*getPotenciaTotal* per tal que no les sumi.

El mètode *toString* d'*Hangar* retornarà una cadena amb el següent format:

```
<nom>; capacitat: <capacitat màxima>, pes: <pes>
```

### Correcció del sagnat

Com que ara podem tenir components dins de naus que a la vegada són en un
hangar d'una altra nau, volem que la sortida del mètode *descriu*
quedi correctament sagnada, de manera que es reflecteixi l'estructura de la
nau, com en l'exemple. Modifica el mètode *descriu* de *ComponentCompost*
per fer que actuï així. Nota que no es pot modificar la classe *ComponentNau*.

### Classe *Principal*

Finalment, a la classe *Principal*, implementarem el mètode *main*,
que crearà alguna nau amb algun motor i hangar, que a la vegada, contingui
una altra nau.

***Exemple:***
```
Nau Creuer interestel·lar Aníbal; capacitat: 10, pes: 1000 amb contingut:
  Hangar; capacitat: 200, pes: 80 amb contingut:
    Nau Caça cicló; capacitat: 2, pes: 75 amb contingut:
      Motor; potència: 200, pes: 30
    Final contingut de Caça cicló
  Final contingut de Hangar
  Habitacle; habitants màxims: 50, pes: 120
  Motor auxiliar; potència: 1500, pes: 50
  Motor; potència: 2500, pes: 50
Final contingut de Creuer interestel·lar Aníbal

Potència total: 4000
Pes total: 1405

Nau Caça cicló; capacitat: 2, pes: 75 amb contingut:
  Motor; potència: 200, pes: 30
Final contingut de Caça cicló

Potència total: 200
Pes total: 105
```

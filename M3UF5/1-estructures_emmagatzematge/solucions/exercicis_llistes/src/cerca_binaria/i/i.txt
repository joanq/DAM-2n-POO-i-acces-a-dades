Creus que hi pot haver algun problema amb la cerca binària si es modifiquen
els valors dels atributs dels objectes que hi ha guardats a la llista?
Llegeix la introducció de la interfície Set a la documentació de les API
i fixa't en la nota que hi ha sobre elements mutables. Per què creus que hi
ha aquesta advertència?

- Sí. Si es modifiquen les variables internes dels objectes que hi ha
guardats a la llista, pot passar que un objecte "creixi" o es faci "petit"
segons el seu ordre natural. Llavors, la llista deixaria d'estar ordenada
i els mètodes fallarien.
Per exemple, suposem la classe Persona que implementa Comparable fent que
es comparin les persones segons l'edat. Si posem vàries persones de 20 anys
a la llista i després augmentem un any a la persona que ocupa el primer lloc
(perquè és el seu aniversari), la llista queda:
[21][20][20][20][20]
i, com es pot veure, no està ordenada. A partir d'aquí, els mètodes add, remove
i binarySearch començarien a fer coses estranyes.

- El mateix problema que hi ha amb la LlistaOrdenada passa a Set. El canvi de
les variables internes pot fer que canviï el resultat de l'equals en alguns
casos, i que en el conjunt hi passin a haver elements repetits, cosa que no
hauria de passar mai.
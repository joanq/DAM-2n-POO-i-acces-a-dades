package exercicis_piles_cues;

import java.util.Deque;
import java.util.LinkedList;

public class IntABinari {
	public static String aBinari(int n) throws IllegalArgumentException {
		Deque<Integer> pila = new LinkedList<>();
		if (n<0)
			throw new IllegalArgumentException("El nombre ha de ser positiu.");
		String resultat = (n==0?"0":"");
		while (n>=1) {
			pila.addFirst(n%2);
			n/=2;
		}
		while (!pila.isEmpty()) {
			resultat+=pila.removeFirst().toString();
		}
		return resultat;
	}

	public static void main(String[] args) {
		for (int i=0; i<=20; i++) {
			System.out.println(i+" en binari és "+ aBinari(i));
		}
		try {
			aBinari(-1);
		} catch (IllegalArgumentException e) {
			System.out.println("Números negatius ok");
		}
	}
}

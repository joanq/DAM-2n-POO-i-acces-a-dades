package exercicis_piles_cues;

import java.util.Deque;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

public class MostraPerOrdre {
	private Deque<Double> pila = new LinkedList<>();
	private Deque<Double> cua = new LinkedList<>();
	
	public void mostraPerOrdre() {
		while (!cua.isEmpty()) {
			System.out.println(cua.removeLast());
		}
	}
	
	public void mostraAlReves() {
		while (!pila.isEmpty()) {
			System.out.println(pila.removeFirst());
		}
	}
	
	public void afegeixNum(double n) {
		pila.addFirst(n);
		cua.addFirst(n);
	}
	
	public void demanaNums(Scanner sc) {
		double n=-1;
		System.out.println("Introdueix nombres (0 per acabar): ");
		do {
			try {
				n = sc.nextDouble();
				if (n!=0) {
					afegeixNum(n);
				}
			} catch (InputMismatchException e) {
				sc.next();
			}
		} while (n!=0);
	}

	public static void main(String[] args) {
		MostraPerOrdre mostra = new MostraPerOrdre();
		try (Scanner sc = new Scanner(System.in)) {
			mostra.demanaNums(sc);
		}
		System.out.println("Nombres per ordre:");
		mostra.mostraPerOrdre();
		System.out.println("Nombres al revés:");
		mostra.mostraAlReves();
	}
}

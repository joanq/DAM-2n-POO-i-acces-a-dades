### Quatre en ratlla

En el quatre en ratlla dos jugadors competeixen per aconseguir quatre o més
fitxes alineades (en horitzontal, vertical o diagonal). El tauler és vertical,
de manera que els jugadors trien, per torns, a quina columna deixen anar
una fitxa del seu color, i la fitxa cau fins a la posició lliure inferior.

El joc acaba quan un jugador aconsegueix unir quatre fitxes dels seu color en
línia, o bé quan s'ha omplert tot el tauler.

![Joc a mitja partida](../imatges/four_in_a_row1.png)

Les caselles amb fons negre són les que estan lliures. Les que tenen el fons
blau són d'un jugador, i les que tenen el fons verd són de l'altre.

Els jugadors tiren utilitzant els botons de baix, i la part de la dreta informa
de a quin jugador li toca.

El botó *Nova partida* buida completament el tauler i prepara el joc per una
partida nova, tant si la partida anterior ha finalitzat com si no.

Els botons i les caselles tenen una mida de 50x50 píxels. Els botons es
desactiven quan una columna és completament plena o quan s'ha acabat la
partida, de manera que és impossible seguir jugant a aquella columna.

El programa detecta quan un jugador guanya, o quan s'ha omplert tot el tauler
i es produeix, per tant, un empat.

![Joc al final](../imatges/four_in_a_row2.png)

El programa s'ha dividit entre la vista i el model. A la part de la vista
tenim la classe principal, *FourInARow*. Aquesta classe és la responsable de:

- Crear i organitzar els controls de la finestra principal.
- Enllaçar els controls amb les parts del model que corresponen.
- Respondre als esdeveniments dels botons, notificant al model dels canvis
que s'han de produir.

A la part de la vista tenim dues classes més: *BoardSquare* que representa
cadascuna de les caselles del tauler, i *PlayButton* que personalitza el
comportament dels botons de joc.

Pel que fa al model, la classe principal és *Game*. Aquesta classe conté
una colla de propietats que mantenen l'estat del joc:

- *turn* que indica a quin jugador li toca jugar.
- *board* que és una matriu que representa el tauler de joc. A cada posició
de *board* s'hi guarda el propietari de la casella.
- *colFills* que recorda quantes caselles queden lliures a cada columna.
- *nTurns* que guarda el nombre total de torns que s'han jugat, i que permet
identificar ràpidament les situacions d'empat.
- i *winner* que guarda el jugador que ha guanyat la partida, si ho ha fet
algun.

A banda de tots els mètodes *get* i *set* d'aquestes propietats, aquesta
classe proporciona mètode per començar una nova partida, per fer una jugada, i
per detectar les situacions de final de partida.

A la part del model també tenim una enumeració auxiliar, *Owner*, que
s'utilitza per indicar la propietat de cada casella, la possessió del torn, i
la victòria dels jugadors.

Es proporciona un
[codi a mitges](../codi/javafxFourInARowEnunciat). Cal
localitzar els diversos *TODO* que apareixen en el codi i anar completant
les parts que hi falten segons les indicacions.

Si us encalleu, també podeu consultar la
[solució](../codi/javafxFourInARow).

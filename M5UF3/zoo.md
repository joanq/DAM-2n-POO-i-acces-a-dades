Problema del zoo
----------------
Volem desenvolupar un programa que simularà un zoo en línia de comandes.

El programa demanarà entrada per teclat per part de l'usuari. Acceptarà les
següents ordres:

- *afegeix &lt;animal>*, on *animal* és el nom d'algun dels animals que hi haurà al
zoo. S'afegirà al zoo un animal del tipus indicat.
- *suprimeix &lt;animal>*, eliminarà un animal del tipus indicat del zoo.
- *suprimeix tots &lt;animal>*, eliminarà tots els animals del zoo del tipus
indicat.
- *mira*, després d'aquesta ordre es mostrarà un missatge per pantalla que
indicarà què està passant al zoo.
- *surt*, sortirà del programa.

Després de qualsevol ordre, el programa farà l'acció que pertoqui i demanarà
una altra ordre a l'usuari, excepte en el cas de l'ordre *surt*.

Quan s'executa l'ordre *mira* poden succeir diverses coses. Hi ha un 50% de
possibilitats que l'acció que es mostri la faci un espectador del zoo, i un 50%
que la faci un dels animals del zoo.

Si l'acció la fa un espectador, es triarà un animal aleatori del zoo i es
mostrarà un text indicant que un espectador observa a aquell animal.

Si l'acció la fa un animal, es triarà un animal aleatori i l'acció dependrà de
quin animal ha estat elegit.

Les accions que pot fer un animal són tres: moure's, expressar-se, o
alimentar-se. Quina acció es fa en cada cas es tria aleatòriament (totes tenen
la mateixa probabilitat).

En el nostre zoo hi haurà dos tipus d'animals, vaques i cocodrils, però n'hi
podria haver més en un futur.

El comportament dels animals és el següent:

**Vaca:**

- Si es mou, es mostrarà la cadena "*Una vaca es posa a dormir*".
- Si s'alimenta, es buscarà un altre animal aleatòriament. Si l'animal és una
vaca, es mostrarà el text "*Una vaca pasta al costat d'una altra vaca*". Si és
un cocodril es mostrarà "*Una vaca pasta al costat d'un perillós cocodril!*".
Si no hi hagués animals, o el segon animal aleatori fos la mateixa vaca, es
mostraria "*Una vaca pasta solitària*".
- Si s'expressa, es mostrarà la cadena "*Una vaca fa mu*".

**Cocodril:**

- Si es mou, es mostrarà la cadena "*Un cocodril neda estany amunt, estany
avall*".
- Si s'alimenta, es cerca un altre animal aleatòriament. L'altre animal
s'elimina del zoo i es mostra el missatge "*Un cocodril es menja una vaca*" o
"*Un cocodril es menja un altre cocodril!*", segons sigui el cas. Si no hi ha
cap altre animal, o el segon animal és el propi cocodril, es mostrarà el
text "*Un cocodril busca a qui es pot menjar*".
- Si s'expressa, es mostrarà el text "*Un cocodril obre una boca plena de
dents*".

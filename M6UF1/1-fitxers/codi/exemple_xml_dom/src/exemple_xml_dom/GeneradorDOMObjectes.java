package exemple_xml_dom;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

public class GeneradorDOMObjectes {
	private List<Mascota> mascotes = new ArrayList<Mascota>();

	public static void main(String[] args) {
		GeneradorDOMObjectes gen = new GeneradorDOMObjectes();
		try {
			gen.guardaObjectes();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public GeneradorDOMObjectes() {
		mascotes.add(new Mascota("Rudy", 4));
		mascotes.add(new Mascota("Piolin", 2, false));
		mascotes.add(new Mascota("Nemo", 0, false));
		mascotes.add(new Mascota("Tara", 8));
	}
	
	public void guardaObjectes() throws IOException {
		try (FileWriter writer = new FileWriter("mascotes.xml")) {
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			DOMImplementation domImplementation = documentBuilder.getDOMImplementation();
			Document document = domImplementation.
					createDocument("http://www.elmeunamespace.com",
							"mascotes", null);
			document.setXmlVersion("1.0");

			for (Mascota mascota : mascotes) {			
				Element arrel = document.createElement("mascota");
				document.getDocumentElement().appendChild(arrel);
				
				crearElement("nom", mascota.getNom(), arrel, document);
				arrel.setAttribute("num_potes", Integer.toString(mascota.getNumPotes()));
				crearElement("te_pel", Boolean.toString(mascota.hasPel()), arrel, document);
			}
			Source source = new DOMSource(document); 
			Result result = new StreamResult(writer);

			Transformer transformerFactory= TransformerFactory.newInstance().newTransformer();
			transformerFactory.setOutputProperty(OutputKeys.INDENT, "yes");
			transformerFactory.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			transformerFactory.transform(source, result);
			
			Result console = new StreamResult(System.out);
			transformerFactory.transform(source, console);
		} catch (ParserConfigurationException | TransformerException | IOException e) { 
			throw new IOException(e);
		}
	}
	
	private static void crearElement(String dada, String valor, Element arrel, Document document) {
		Element e = document.createElement(dada);
		Text text = document.createTextNode(valor);
		arrel.appendChild(e);
		e.appendChild(text);	
	}
}

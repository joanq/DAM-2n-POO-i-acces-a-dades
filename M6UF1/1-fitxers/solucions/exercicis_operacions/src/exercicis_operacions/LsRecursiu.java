package exercicis_operacions;

import java.io.IOException;
import java.nio.file.DirectoryIteratorException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Deque;
import java.util.LinkedList;

public class LsRecursiu {
	private static Deque<Path> pila = new LinkedList<Path>();
	
	public static void main(String[] args) {	
		if (args.length>0) {
			String directori = args[0];
			Path dir = Paths.get(directori);
			pila.push(dir);
			while (!pila.isEmpty()) {
				try {
					mostraInfoDirectori(pila.pop());
				} catch (IllegalArgumentException e) {
					System.err.println(e.getMessage());
				}
			}
		} else
			System.err.println("S'esperava un nom de directori");
	}
	
	private static void mostraInfoDirectori(Path dir) {
		if (Files.isDirectory(dir)) {
			System.out.println("Fitxers del directori "+dir);
			try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
				for (Path f: stream) {
					if (Files.isDirectory(f))
						pila.push(f);
					mostraInfoFitxer(f);
				}
			} catch (IOException | DirectoryIteratorException ex) {		    
				System.err.println(ex);
			}
			System.out.println();
		} else
			throw new IllegalArgumentException(dir+": No és un directori");
	}
	
	private static void mostraInfoFitxer(Path f) {
		System.out.print(Files.isDirectory(f)?"d":"-");
		System.out.print(Files.isReadable(f)?"r":"-");
		System.out.print(Files.isWritable(f)?"w":"-");
		System.out.print(Files.isExecutable(f)?"x":"-");
		System.out.println(" "+f.getFileName());
	}
}

package mongodb.aggregation;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Aggregates.unwind;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;

public class ExAggregation2 {
	private static int lastId = -1;

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("students");

		/*
		 * Noteu que agafar l'id al fer el $group és important, perquè hi ha
		 * alumnes amb noms duplicats.
		 db.students.aggregate([
		 	{$unwind:"$scores"},
		 	{$group:{
		 		_id:{id:"$_id", name:"$name",type:"$scores.type"},
		 		quantitat:{$sum:1}}},
		 	{$sort:{"_id.name":1, "_id.id":1}}])
		 */
		coll.aggregate(Arrays.asList(
			unwind("$scores"),
			group(new Document("id","$_id").append("name", "$name").append("type", "$scores.type"), sum("quantitat", 1)),
			sort(Sorts.ascending("_id.name", "_id.id"))
		)).forEach((Document doc) -> {
			Document id = doc.get("_id", Document.class);
			if (lastId!=id.getInteger("id")) {
				lastId = id.getInteger("id");
				System.out.println("Alumne "+id.getInteger("id")+" "+id.getString("name"));
			}
			System.out.println("  Tipus: "+id.getString("type")+"  Quantitat: "+doc.getInteger("quantitat"));
		});
		client.close();

	}

}

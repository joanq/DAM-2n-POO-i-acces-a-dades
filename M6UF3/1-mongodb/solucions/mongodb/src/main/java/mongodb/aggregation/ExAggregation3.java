package mongodb.aggregation;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.unwind;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExAggregation3 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("students");
		
		/*
		 db.students.aggregate([
		 	{$unwind:"$scores"},
		 	{$group:{
		 		_id:"$scores.type",
		 		mitjana:{$avg:"$scores.score"}}}
		 ])
		 */
		coll.aggregate(Arrays.asList(
			unwind("$scores"),
			group("$scores.type", avg("mitjana", "$scores.score"))
		)).forEach((Document doc)-> {
			System.out.println("Tipus d'activitat: "+doc.getString("_id")+"  Nota mitjana: "+doc.getDouble("mitjana"));
		});
		client.close();
	}

}

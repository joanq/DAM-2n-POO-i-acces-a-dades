package mongodb.aggregation;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Sorts.ascending;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExAggregation4 {
	private Student student;

	public static void main(String[] args) {
		new ExAggregation4().run();
	}
	
	public void run() {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("students");
		
		/*
		 db.students.aggregate([
		 	{$unwind:"$scores"},
		 	{$group:{
		 		_id:{id:"$_id",name:"$name",type:"$scores.type"},
		 		score:{$avg:"$scores.score"}}},
		 	{$sort:{"_id.id":1}}
		 ])
		 */
		coll.aggregate(Arrays.asList(
			unwind("$scores"),
			group(
					new Document("id","$_id").append("name", "$name").append("type", "$scores.type"),
					avg("score", "$scores.score")),
			sort(ascending("_id.id"))
		)).forEach((Document doc)->{
			Document id = doc.get("_id", Document.class);
			if (student==null || student.id != id.getInteger("id")) {
				if (student!=null)
					System.out.println(student.toString());
				student = new Student(id.getInteger("id"), id.getString("name"));
			}
			String type = id.getString("type");
			double sc = doc.getDouble("score");
			if (type.equals("quiz"))
				student.scoreQuiz=sc;
			else if (type.equals("homework"))
				student.scoreHomework=sc;
			else if (type.equals("exam")) {
				student.scoreExam=sc;
			}
		});
		System.out.println(student.toString());
		
		client.close();
	}
}

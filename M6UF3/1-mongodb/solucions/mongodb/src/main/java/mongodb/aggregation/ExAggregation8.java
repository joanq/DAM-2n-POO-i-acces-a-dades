package mongodb.aggregation;

import static com.mongodb.client.model.Accumulators.addToSet;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.sort;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;

public class ExAggregation8 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("grades");

		coll.aggregate(Arrays.asList(
			group("$class_id",
			addToSet("students", "$student_id")),
			sort(Sorts.ascending("_id"))
		)).forEach((Document doc) -> {
			System.out.println("Classe "+doc.getInteger("_id"));
			@SuppressWarnings("unchecked")
			List<Integer> students = (List<Integer>) doc.get("students");
			Collections.sort(students);
			System.out.println("Estudiants:");
			for (Integer id : students) {
				System.out.print(id+" ");
			}
			System.out.println();
		});
		
		client.close();
	}

}

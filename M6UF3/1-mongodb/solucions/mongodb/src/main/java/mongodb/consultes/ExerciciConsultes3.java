package mongodb.consultes;

import static com.mongodb.client.model.Sorts.ascending;

import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExerciciConsultes3 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("zips");
		
		int count = (int)coll.count();
		
		Document doc = coll.find().sort(ascending("loc.1")).skip(count/2).first();
		@SuppressWarnings("unchecked")
		List<Double> loc = (List<Double>) doc.get("loc");
		System.out.println(doc.getInteger("_id")+": "+doc.getString("city")+", "+doc.getString("state")+
				" ("+loc.get(0)+", "+loc.get(1)+")");
		
		client.close();
	}

}

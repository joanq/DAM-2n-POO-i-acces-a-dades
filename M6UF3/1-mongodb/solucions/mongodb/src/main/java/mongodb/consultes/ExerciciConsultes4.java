package mongodb.consultes;

import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Sorts.ascending;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExerciciConsultes4 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("zips");
		
		coll.find(lt("pop", 50)).sort(ascending("city")).projection(fields(include("city", "pop"), excludeId())).forEach(
				(Document doc) -> System.out.println("City: "+doc.getString("city")+" - Pop: "+doc.getInteger("pop")));

		client.close();
	}

}

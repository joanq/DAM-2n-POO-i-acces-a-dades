package mongodb.consultes;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.descending;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExerciciConsultes5 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("zips");
		
		List<String> states = coll.distinct("state", String.class).into(new ArrayList<String>());
		Collections.sort(states);
		for (String state : states) {
			Document first = coll.find(eq("state", state)).sort(ascending("_id")).first();
			Document last = coll.find(eq("state", state)).sort(descending("_id")).first();
			System.out.println("Estat "+state);
			System.out.println(" Codi postal més baix: "+first.getInteger("_id")+" ("+first.getString("city")+")");
			System.out.println(" Codi postal més alt: "+last.getInteger("_id")+" ("+last.getString("city")+")");
		}
		client.close();
	}
}

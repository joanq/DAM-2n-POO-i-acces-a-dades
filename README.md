Materials per als mòduls *M3 - Programació orientada a objectes*, *M5 - Entorns de desenvolupament (disseny orientat a objectes)*, i *M6 - Accés a dades* del cicle formatiu de grau superior *Desenvolupament d'Aplicacions Multiplataforma*.
====

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />
<span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Materials per als mòduls M3 - Programació orientada a objectes, M5 - Entorns de desenvolupament (disseny orientat a objectes), i M6 - Accés a dades del cicle de Desenvolupament d'Aplicacions Multiplataforma</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades" property="cc:attributionName" rel="cc:attributionURL">https://gitlab.com/joanq/DAM-2n-POO-i-acces-a-dades</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Índex
-----

[Configuració de l'entorn de desenvolupament](entorn.adoc)

### M3UF4: programació orientada a objectes (POO). Fonaments.

1. [Utilització avançada de classes](M3UF4/1-classes)

### M3UF5: POO. Llibreries de classes fonamentals.

1. Estructures d'emmagatzematge
  - [Teoria](M3UF5/1-estructures_emmagatzematge/readme.adoc)

  Activitats:

  - [Exercicis](M3UF5/1-estructures_emmagatzematge/exercicis.adoc)
  - [Exercici del domino](M3UF5/1-estructures_emmagatzematge/exercicis/domino.md)
  - [Exercici de la fàbrica de naus](M3UF5/1-estructures_emmagatzematge/exercicis/fabrica_naus.md)
  - [Exercici de la pizzera d'en Luigi](M3UF5/1-estructures_emmagatzematge/exercicis/pizzeria.md)

  Solucions:

  - [Exercicis de piles i cues](M3UF5/1-estructures_emmagatzematge/solucions/exercicis_piles_cues)
  - [Exercicis de llistes](M3UF5/1-estructures_emmagatzematge/solucions/exercicis_llistes)
  - [Exercici de l'agenda](M3UF5/1-estructures_emmagatzematge/solucions/agenda)
  - [Exercicis de conjunts](M3UF5/1-estructures_emmagatzematge/solucions/exercicis_conjunts)
  - [Exercicis de diccionaris](M3UF5/1-estructures_emmagatzematge/solucions/exercicis_diccionaris)
  - [Exercici del domino](M3UF5/1-estructures_emmagatzematge/solucions/domino)
  - [Exercici de la fàbrica de naus](M3UF5/1-estructures_emmagatzematge/solucions/fabrica_naus)
  - [Exercici de la pizzera d'en Luigi](M3UF5/1-estructures_emmagatzematge/solucions/pizzes)

2. Interfícies gràfiques
  - [Teoria](M3UF5/2-interficies_grafiques/readme.adoc)

  Activitats:

  - [Exercici Observer Pattern](M3UF5/2-interficies_grafiques/exercici_observer.md)
  - [Exercici quatre en ratlla](M3UF5/2-interficies_grafiques/exercicis/quatre_en_ratlla.md)
  - [Exercici xinos](M3UF5/2-interficies_grafiques/exercicis/xinos.md)

### M3UF6: POO. Introducció a la persistència en BD.

1. Accés a bases de dades amb JDBC
  - [Teoria](M3UF6/1-jdbc/readme.adoc)
  - [Exercicis](M3UF6/1-jdbc/exercicis.md)

  Solucions:

  - [Consultes estàtiques](M3UF6/1-jdbc/solucions/consultes_estatiques)
  - [Consultes amb paràmetres](M3UF6/1-jdbc/solucions/consultes_parametres)
  - [Metadades](M3UF6/1-jdbc/solucions/metadades)
  - [Modificació de dades](M3UF6/1-jdbc/solucions/modificacio_dades)

### M5UF3: introducció al disseny orientat a objectes.

1. Diagrames UML
  - [Teoria](M5UF3/readme.adoc)
  - [Exercicis](M5UF3/exercicis.adoc)

### M6UF1: persistència en fitxers.

1. Persistència en fitxers
  - [Teoria](M6UF1/1-fitxers/readme.adoc)
  - [Exercicis](M6UF1/1-fitxers/exercicis.md)

  Solucions:

  - [Exercicis operacions sobre fitxers](M6UF1/1-fitxers/solucions/exercicis_operacions)
  - [Exercicis fitxers binaris](M6UF1/1-fitxers/solucions/fitxers_binaris)
  - [Exercicis fitxers d'objectes](M6UF1/1-fitxers/solucions/fitxers_objectes)
  - [Exercicis random access files](M6UF1/1-fitxers/solucions/exercicis_raf)
  - [Exercicis fitxers de text](M6UF1/1-fitxers/solucions/exercicis_text)
  - [Exercicis fitxers XML](M6UF1/1-fitxers/solucions/exercicis_xml)

### M6UF2: persistència en BDR-BDOR-BDOO.

1. Java Server Faces
  - [Teoria](M6UF2/1-jsf/readme.adoc)

2. Hibernate
  - [Teoria](M6UF2/2-hibernate/readme.adoc)

### M6UF3: persistència en BD natives XML.

1. Persistència en BD orientades a documents
  - [Teoria](M6UF3/1-mongodb/readme.adoc)
  - [Exercicis](M6UF3/1-mongodb/exercicis.md)
  - [Solucions](M6UF3/1-mongodb/solucions)

### M6UF4: components d’accés a dades.

1. [Creació d'aplicacions web amb un framework](M6UF4/1-laravel/readme.adoc)
